# rspam
#
# @summary Init class for rspam module, includes other classes
#
class rspamd {

  include rspamd::install
  include rspamd::config
  include rspamd::service

}
