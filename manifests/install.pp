# @summary
#   This class handles the rspam install.
#
# @api private
#
class rspamd::install (

  $rspamd_packages = hiera('rspam_packages'),

  ) inherits rspamd {

  $rspamd_packages.each |String $package|{
    package { $package:
      ensure  =>  installed,
    }
  }

}
